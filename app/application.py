import os
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template
import pandas as pd

application = Flask(__name__)
application.config.from_object(os.environ['APP_SETTINGS'])
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(application)


@application.route('/')
def index():
    connection = db.engine.connect()
    data = pd.read_sql_table('results', con=connection)
    return render_template('index.html', tables=[data.to_html(classes='data')], titles=data.columns.values)


@application.route('/hello/')
@application.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)


if __name__ == '__main__':
    application.run()
