import os
from flask import Flask
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand

from util.commands import Adhoc

application = Flask(__name__)
application.config.from_object(os.environ['APP_SETTINGS'])

application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(application)
from models import *

migrate = Migrate(application, db)
manager = Manager(application)

manager.add_command('db', MigrateCommand)
manager.add_command('adhoc', Adhoc(db))

if __name__ == '__main__':
    manager.run()
