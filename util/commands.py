from flask_script import Command


class Adhoc(Command):
    """A command to run adhoc sql"""

    def __init__(self, db):
        self.db = db

    def run(self):
        with open('../db/adhoc.sql') as file:
            with self.db.engine.connect() as con:
                query = file.read()
                rs = con.execute(query)
                print(query)