### Setting Environments
```commandline
export APP_SETTINGS="config.ProductionConfig"
```

```commandline
export APP_SETTINGS="config.StagingConfig"
```

```commandline
export APP_SETTINGS="config.DevelopmentConfig"
```

```commandline
export APP_SETTINGS="config.TestingConfig"
```

### Initialise Migration
```commandline
export DATABASE_URL="postgresql:///wordcount_dev"
python manage.py db init
```

### Run Migration
```commandline
python manage.py db migrate
python manage.py db upgrade
```

### Run Adhoc Query
```commandline
python manage.py adhoc
```